<?php

/**
 * Interfaces
 * @author Sebastian Skejø
 */

/**
 */
interface AOOSStorageInterface
{
    /**
     * Sets table to manipulate data in
     */
    public function setTable($table);

    /**
     * Inserts values into database
     */
    public function insert($values);

    /**
     * Removes values from database
     */
    public function remove($where);

    /**
     * Updates values in database
     */
    public function update($values, $where, $limit = 0);

    /**
     * Returns an array with all fields $fields that matches $where
     */
    public function select($fields, $where, $order, $limit);

    /**
     * Returns an integer representing the number of rows
     */
    public function numRows($where = null);

    /**
     * Creates a table
     */
    public function create($fields, $props);

    /**
     * Drops a table
     */
    public function drop();
}

/**
 * AOOSModelPropertyInterface
 */
interface AOOSModelConstraintInterface {
    static public function name();

    static public function numArgs();

    static public function check($args, $value);

    static public function execute($args, $value);
}
?>
