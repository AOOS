<?php
require_once("AOOSException.php");
require_once("AOOSModule.php");
require_once("AOOSInterfaces.php");

/**
 * The mysql-part of AOOSStorageDevice
 * @author Sebastian Skejø
 */
class AOOSMysqlInterface extends AOOSModule implements AOOSStorageInterface
{
    private $_connection    = null;
    private $_username      = null;
    private $_password      = null;
    private $_database      = null;
    private $_host          = null;
    private $_table         = null;
    private $_query         = null;
    private $_queryText     = null;

    public function __construct($core)
    {
        parent::__construct($core);

        try {
            $this->_username    = $core->getSetting("DBUsername");
            $this->_password    = $core->getSetting("DBPassword");
            $this->_database    = $core->getSetting("DBDatabase");
            $this->_host        = $core->getSetting("DBHost");
        }
        catch (AOOSException $e)
        {
        }

        $this->_connect();
    }

    public function dataModelDefinition() {
        return 0;
    }

    private function _connect() {
        if (!$this->_connection = mysql_connect($this->_host, $this->_username, $this->_password))
            throw new AOOSException($this->core(), $this->tr("mysql_connect_fail"), $this->tr("error").": ".mysql_error());
        if (!mysql_select_db($this->_database))
            throw new AOOSException($this->core(), $this->tr("mysql_select_db_fail"), $this->tr("error").": ".mysql_error());
    }

    public function __wakeup() {
        $this->_connect();
        $this->_queryText = null;
    }

    private function _makeQuery($query)
    {
        $this->_queryText = $query;
        if (!($this->_query = mysql_query($this->_queryText))) {
            $e = new AOOSException($this->core(), $this->tr("mysql_query_fail"), $this->tr("error").": ".mysql_error()."<br />Query: ".$query, true, 0);
            return false;
        }
        $this->core()->log($query);

        return true;
    }

    public function setTable($table)
    {
        $this->_table = $table;
    }

    public function setQuery($query)
    {
        return $this->_makeQuery($query);
    }

    /** Inserts an array
     * @param string $field Field name
     * @param string $value Value
     * @return bool
     */
    public function insert($values)
    {
        $rawQuery   = "INSERT INTO %s (%s) VALUES (%s)";
        $fields = implode(",", array_keys($values));
        $vals = implode(",", array_values($values));
        $query = sprintf($rawQuery, $this->_table, $fields, $vals);
        
        if (!$this->_makeQuery($query)) {
            return false;
        }
        return true;
    }

    /**
     * Selects a single row
     */
    public function select($fields, $where, $order, $limit)
    {
        $fields = implode(",", $fields);
        $raw = "SELECT %s FROM %s";
        $query = sprintf($raw, $fields, $this->_table);

        if ($w = $this->_arrayToWhere($where)) {
            $query .= " WHERE ".$w;
        }

        if ($l = $this->_arrayToLimit($limit)) {
            $query .= " LIMIT ".$l;
        }

        if ($o = $this->_arrayToOrder($order)) {
            $query .= " ORDER BY ".$o;
        }

        try {
            $this->_makeQuery($query);
        } catch (AOOSException $e) {
            throw $e;
        }
        $a = array();
        while ($row = mysql_fetch_assoc($this->_query)) {
            $a[] = $row;
        }
        mysql_free_result($this->_query);
        return $a;
    }

    /**
     * Update values
     * @param $values
     * @param $where
     * @param $limit
     * @return bool
     */
    public function update($values, $where, $limit = null)
    {
        $raw = "UPDATE %s SET %s WHERE %s";
        if (!($v = $this->_arrayToWhere($values))) {
            return false;
        }
        $v = str_replace("AND", ",", $v);
        if (!($w = $this->_arrayToWhere($where))) {
            return false;
        }
        if ($l = $this->_arrayToLimit($limit)) {
            $raw .= $l;
        }
        
        $query = sprintf($raw, $this->_table, $v, $w);
        return $this->_makeQuery($query);
    }

    /**
     * Deletes a row.
     * @param string $where
     * @return bool
     */
    public function remove($where, $limit = null)
    {
        $raw = "DELETE FROM %s WHERE %s";
        if (!($w = $this->_arrayToWhere($where))) {
            return false;
        }
        if ($l = $this->_arrayToLimit($limit)) {
            $raw .= " LIMIT ".$l;
        }
        $query = sprintf($raw, $this->_table, $w);

        return $this->_makeQuery($query);
    }

    /** XXX
     * Num rows
     * @return int
     */
    public function numRows($where = null)
    {
        $query = sprintf("SELECT COUNT(*) FROM %s ", $this->_table);
        if ($w = $this->_arrayToWhere($where)) {
            $query .= "WHERE ".$w;
        }
        $this->_makeQuery($query);
        $result = mysql_fetch_array($this->_query);
        return $result[0];
    }

    /**
     * Creates a table in the database
     */
    public function create($fields, $flags) {
        $f = array();
        $nr = 0;
        $query = sprintf("CREATE TABLE IF NOT EXISTS %s (\n", $this->_table);
        foreach ($fields as $field => $type) {
            $nr++;

            $t = $field;
            switch ($type) {
            case(AOOSMODEL_TYPE_STRING):
                $t .= " VARCHAR(255)";
                break;
            case(AOOSMODEL_TYPE_INTEGER):
                $t .= " INT(11)";
                break;
            case(AOOSMODEL_TYPE_TEXT):
                $t .= " TEXT";
                break;
            case(AOOSMODEL_TYPE_BOOLEAN):
                $t .= " BOOL";
                break;
            }
            if ($flags[$field] & AOOSMODEL_FLAG_PRIMARY_KEY) {
                $f[] = sprintf("PRIMARY KEY (%s)", $field);
                $t .= " auto_increment";
            }

            $f[] = $t;
        }
        $query .= implode(",\n", $f);
        $query .= " );";
        if ($nr == 0) {
            return true;
        }
        return $this->_makeQuery($query);
    }

    /**
     * Removes a table
     */
    public function drop() {
        $query = sprintf("DROP TABLE IF EXISTS %s", $this->_table);
        return $this->_makeQuery($query);
    }

    private function _arrayToWhere($array) {
        if (is_array($array)) {
            $a = array();
            foreach ($array as $key => $value) {
                $a[] = $key."=".$value;
            }
            $str = count($a) > 1 ? implode(" AND ", $a) : $a[0];
            return $str;
        }
        return false;
    }

    private function _arrayToLimit($a) {
        if (is_array($a)) {
            if (count($a) == 1) {
                $query = sprintf("%s", $a[0]);
            }
            else {
                $query = sprintf("%s", $a[0], $a[1]);
            }
            return $query;
        }
        return false;
    }

    private function _arrayToOrder($a) {
        if (is_array($a)) {
            $o = sprintf("%s %s", $a[0], $a[1]);
            return $o;
        }
        return false;
    }

}
?>
