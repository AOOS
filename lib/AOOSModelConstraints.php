<?php
require_once("AOOSInterfaces.php");

class LengthConstraint implements AOOSModelConstraintInterface {
    static public function name() {
        return "Length";
    }

    static public function numArgs() {
        return 1;
    }

    static public function check($args, $value) {
        if (is_integer($value) || is_object($value) || is_float($value) || is_bool($value)) {
            return false;
        }
        return true;
    }

    static public function execute($args, $value) {
        $length = $args[0];
        if (is_string($value)) {
            if (strlen($value) > $length) {
                return substr($value, 0, $length);
            }
            else {
                return $value;
            }
        }
        elseif (is_array($value)) {
            if (count($value) > $length) {
                return array_slice($value, 0, $length);
            }
            else {
                return $value;
            }
        }
        return false;
    }
}

class MatchConstraint implements AOOSModelConstraintInterface {
    static public function name() {
        return "Match";
    }

    static public function numArgs() {
        return 1;
    }

    static public function check($args, $value) {
        if (!is_string($value)) {
            return false;
        }
        return true;
    }

    static public function execute($args, $value) {
        $pattern = $args[0];
        if (preg_match($pattern, $value)) {
            return $value;
        }
        return false;
    }
}

class RangeConstraint implements AOOSModelConstraintInterface {
    static public function name() {
        return "Range";
    }

    static public function numArgs() {
        return 2;
    }

    static public function check($args, $value) {
        $start = $args[0];
        $end = $args[1];
        if (!is_integer($value) && !is_float($value)) {
            return false;
        }
        if (!(is_integer($start) || is_float($start) || is_integer($end) || is_float($end)) || $start > $end) {
            return false;
        }
        return true;
    }

    static public function execute($args, $value) {
        $start = $args[0];
        $end = $args[1];
        if ($value < $start) {
            $value = $start;
        }
        elseif ($value > $end) {
            $value = $end;
        }
        return $value;
    }
}
?>
