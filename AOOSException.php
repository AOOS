<?php

AOOSCore::register("AOOSTypeException", "AOOSException.php");

/**
 * AOOSException
 * @author Sebastian Skejø
 */

class AOOSException extends Exception
{
    private $_core = null;
    private $_name = null;
    private $_msg = null;
    private $_trace = null;
    private $_ecode = null;

    /**
     * When an AOOSException is constructed it is automatically set in the given $core
     * @param $core An AOOSCore-object
     * @param $name The name of the exception to throw
     * @param $msg A more detailed description of the exception
     * @param $doTrace If this is true the expection will have a backtrace included in it's output
     * @param $code Error code. 0 = fatal error, 1 = error, 2 = warning.
     */
    public function __construct($core, $name, $msg="", $doTrace=true, $code = 1)
    {
        $this->_core = $core;

        $this->_name = $name;
        $this->_msg = $msg;
        if ($doTrace)
        {
            $this->_trace = debug_backtrace();
        }
        if ($code > 2 || $code < 0)
        {
            throw new AOOSException($core, $core->tr("exception_code_error"));
            return false;
        }
        $this->_ecode = $code;
        $this->core()->setException($this);
    }

    /**
     * Makes sure that it is possible to call
     * \code 
     * print new AOOSException($core, $name);
     * \endcode
     */
    public function __toString()
    {
        $str = "<strong>";
        switch($this->_ecode)
        {
        case(0):
            $str .= "fatal_error";
            break;
        case(1):
            $str .= "error";
            break;
        case(2):
            $str .= "warning";
            break;
        }
        $str .= "</strong>: ".$this->_name."<br /><div style='font-size: 10pt;'>".$this->_msg."</div><br /><div style='font-size: 10pt; color: #aaa'>";
        foreach ($this->_trace as $row)
        {
            if (isset($row["class"]))
            {
                if ($row["class"] == "AOOSException")
                {
                    continue;
                }
                $file = in_array("file", array_keys($row)) ? $row["file"] : "unknown file";
                $line = in_array("line", array_keys($row)) ? $row["line"] : "unknown line";
                $str .= "<strong>".$row["class"]."->".$row["function"]."</strong> in ".$file." on line ".$line."<br />";
            }
        }
        $str .= "</div>";
        return $str;
    }

    /**
     * Returns the name of the exception
     */
    public function getName() { return $this->_name; }
    /**
     * Returns the description of the exception
     */
    public function getMsg() { return $this->_msg; }
    /**
     * Returns the error code of the exception
     */
    public function getEcode() { return $this->_ecode; }
    /**
     * Returns the core object
     */
    public function core() { return $this->_core; }
}

/**
 * A convienience exception class for use when exception should descripe a type error
 * \internal Use of error in documentation is not good?
 */
class AOOSTypeException extends AOOSException {
    /**
     * Takes an AOOSCore, $core, an a $type
     * @param $core An AOOSCore
     * @param $type Name of the type which caused problems
     */
    public function __construct($core, $type, $msg) {
        parent::__construct($core, "Type Exception", $type.$msg);
    }
}

class AOOSLangException extends AOOSException {
    public function __construct($core, $str) {
        parent::__construct($core, "Couldn't find string", "String name: ".$str, true, 2);
    }
}
// vim: number
?>
