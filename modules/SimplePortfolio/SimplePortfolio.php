<?php

class SimplePortfolio extends AOOSModule
{
    private $_count     = null;
    private $_limit     = null;
    private $_offset    = null;
    private $_sortBy    = null;
    private $_order     = null; // ASC or DESC
    private $_entries   = null;

    public function __construct($core)
    {
        parent::__construct($core);

        $s = new AOOSStorageDevice($this->core());
        $s->setStorageType("mysql");
        $s->setTable("SimplePortfolio");

        $this->_entries     = $s->selectModel("*");
        $this->_count       = $s->numRows();
        $this->_limit       = $this->core()->getSetting("limit", __class__);
        $this->_offset      = $this->core()->getSetting("offset", __class__);
        $this->_sortBy      = $this->core()->getSetting("sortBy", __class__);
        $this->_order       = $this->core()->getSetting("order", __class__);
        $this->_entries->sort($this->sortBy(), $this->order());
    }

    public function show()
    {
        $rows = $this->entries()->getRows($this->offset(), $this->offset()+$this->limit(), true);
        return "Simple Portfolio";
    }

    public function setSortBy($field)
    {
        if (!$this->entries()->inColumnIndex($field))
        {
            return false;
        }
        $this->_sortBy = $field;
        return true;
    }

    public function sortBy()
    {
        return $this->_sortBy;
    }

    public function setOrder($order)
    {
        $order = strtoupper($order);
        if ($order != "ASC" && $order != "DESC")
        {
            $this->core()->log("Not asc or desc");
            return false;
        }
        $this->_order = $order;
        return true;
    }

    public function order()
    {
        return $this->_order;
    }

    public function setOffset($offset)
    {
        if (!is_integer($offset))
        {
            throw new AOOSException($this->core(), $this->tr("offset_not_numeric"), "", true, 1);
            return false;
        }
        $this->_offset = $offset;
        return true;
    }

    public function offset()
    {
        return $this->_offset;
    }

    public function setLimit($limit)
    {
        if (!is_integer($limit))
        {
            throw new AOOSException($this->core(), $this->tr("limit_not_numeric"), "", true, 1);
            return false;
        }
        $this->_limit = $limit;
        return true;
    }

    public function limit()
    {
        return $this->_limit;
    }
    public function entries()
    {
        return $this->_entries;
    }
}
