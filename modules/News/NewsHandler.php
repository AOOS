<?php

/**
 * NewsHandler
 * @author Sebastian Skejø
 */

class NewsHandler {
    private $_storageObj    = null;
    private $_sort          = null;

    private function _restore() {
        $this->_storageObj->setTable("News");
        $this->_storageObj->setSort($this->_sort);
    }

    public function __construct($core) {
        parent::__construct($core);

        $this->_storageObj = $this->core()->newStorageDevice();
        $this->_storageObj->setTable("News");
        $this->_storageObj->setSort("DESC");
    }

    public function getNewsList($fields = "*", $where = null, $limit = null, $order = "TIMESTAMP") {
        return $this->_storageObj()->selectModel($fields, $where, $limit, $order);
    }

    /**
     * Returns a model containing the data of the current piece of news
     */
    public function getNews($newsID) {
        $where = array(
            "NEWSID" => $newsID
        );
        
        $m = $this->getNewsList("*", $where);

        return $m;
    }

    /**
     * Returns an AOOSModel containing all data for comments to $newsID
     * @return AOOSModel
     */
    public function getCommentList($newsID, $limit = null) {
        $this->_storageObj->setTable("News_comments");
        $this->_storageObj->setSort("ASC"); // We want comments to be in ascending order

        $where = array(
            "NEWSID" => $newsID
        );

        $model = $this->_storageObj->selectModel("*", $where, $limit, "TIMESTAMP");
        
        $this->_restore();

        return $model;
    }

    public function setSort($sort) {
        if ($this->_storageObj->setSort($sort)) {
            $this->_sort = $sort;
        }
        return true;
    }

    /**
     * Deletes a piece of news and all related comments
     * @param int $newsID The piece of news to be deleted
     */
    public function deleteNews($newsID) {
        if (!$this->core()->getModule("User")->checkLevel("admin")) {
            throw new AOOSException($this->core(), $this->tr("access_denied", "User"), "", true, 1);
            return false;
        }

        $where = array(
            "NEWSID" => $newsID
        );

        $this->_storageObj->deleteFromArray($where);

        // Delete comments
        $this->_storageObj->setTable("News_comments");
        $this->_storageObj->deleteFromArray($where);
        $this->_restore();

        return true;
    }

    /**
     * Updates a piece of news
     */
    public function updateNews(AOOSModel $news) {
        return true;
    }

    /**
     * Creates a new piece of news in the database
     */
    public function createNews(AOOSModel $news) {
        return true;
    }
}
// vim: number
?>
