<?php

/**
 * News-module
 * @author Sebastian Skejø
 */

class News extends AOOSModule
{
    private $_view = null;

    public function __construct($core) {
        parent::__construct($core);
        $view = new NewsView($this);
        $this->_view = $view;
    }

    static public function dependencies() {
        return array("Paginator", "Form", "Reciever");
    }

    static public function files() {
        return array("NewsView" => "NewsView.php");
    }

    public function dataModelDefinition() {
        $m = new AOOSModel($this->core());
        $m->setSource("mysql");
        $m->setTable("News");
        $m->setColumnIndex(array("author", "title", "date", "content", "id"));
        $m->setProperties("id", AOOSMODEL_TYPE_INTEGER, AOOSMODEL_FLAG_FROM_DATABASE|AOOSMODEL_FLAG_PRIMARY_KEY|AOOSMODEL_FLAG_GUI_PRIVATE);
        $m->setProperties("author", AOOSMODEL_TYPE_STRING,
            AOOSMODEL_FLAG_FROM_DATABASE,
            AOOSMODEL_PROP_ESCAPE|
            AOOSMODEL_PROP_NOHTML|
            AOOSMODEL_PROP_STRIP);
        $m->setProperties("title", AOOSMODEL_TYPE_STRING,
            AOOSMODEL_FLAG_FROM_DATABASE,
            AOOSMODEL_PROP_ESCAPE|
            AOOSMODEL_PROP_NOHTML|
            AOOSMODEL_PROP_STRIP);
        $m->setProperties("date", AOOSMODEL_TYPE_INTEGER,
            AOOSMODEL_FLAG_FROM_DATABASE|AOOSMODEL_FLAG_GUI_PRIVATE,
            AOOSMODEL_PROP_TIME);
        $m->setProperties("content", AOOSMODEL_TYPE_TEXT,
            AOOSMODEL_FLAG_FROM_DATABASE,
            AOOSMODEL_PROP_ESCAPE|
            AOOSMODEL_PROP_NOHTML);
        return $m;
    }

    public function show() {
        $this->dataModel()->populate(null, array("id", "DESC")); // Get all fields ordered descending by id
        $p = $this->core()->getModule("Paginator");
        switch($p->getOption(0)) {
        case("add"):
            return $this->_view->add();
            break;
        case("show"):
            return $this->_view->show();
            break;
        default:
            return $this->_view->noargs();
            break;
        }
    }

    public function addNews($news) {
        $this->dataModel()->appendRow($news);
        $this->dataModel()->save();
        $this->emit("newsAdded", $news->title);
        WidgetInterface::addMessage($news->title."' added!'");
        return true;
    }

    /**
     * @param $newsid The news id
     */
    public function deleteNews($newsid) {
        $this->dataModel()->remove($newsid);
        $this->dataModel()->save();
        $this->emit("newsDeleted", $newsid);
        return true;
    }
}

class NewsView extends AOOSModule {
    static public function dependencies() {
        return array("Form", "Reciever", "Paginator");
    }

    public function add() {
        $f = $this->core()->getModule("Form")->getWidget("Form");
        $r = $this->core()->getModule("Reciever");
        $p = $this->core()->getModule("Paginator");

        if ($row = $r->getRow("POST")) {
            $this->parent()->addNews($row);
        }

        $ignorecols = array("id");
        $f->setAction($p->currentURL());
        $f->setMethod("POST");
        $f->setModel($this->parent()->dataModel());
        return $f;
    }

    public function show() {
        $p = $this->core()->getModule("Paginator");
        $f = $this->core()->getModule("Form");
        $l = $f->getWidget("List");
        $modpath = $this->core()->getSetting("module_dir");
        $l->setStyle(file_get_contents($modpath.'News/news_show.tmpl'));
        return $l->strRow($this->parent()->dataModel()->find(array("id" => $p->getOption(1))));
    }

    public function noargs() {
        $p = $this->core()->getModule("Paginator");
        $f = $this->core()->getModule("Form");
        $l = $f->getWidget("List");
        $modpath = $this->core()->getSetting("module_dir");
        $l->setStyle(file_get_contents($modpath.'News/news_default.tmpl'));
        $l->setModel($this->parent()->dataModel());
        return $l;
    }
}
// vim: number
?>
