<?php

class Form extends AOOSModule {
    private $_action        = "";
    private $_method        = "";
    private $_submitSignal  = "";
    private $_submitArgs    = array();

    public function getWidget($type) {
        $name = $type."Widget";
        return new $name();
    }

    static public function files() {
        return array(
            "Widget"        => "Widget.php",
            "ListWidget"    => "ListWidget.php",
            "FormWidget"    => "FormWidget.php",
            "TableWidget"   => "TableWidget.php");
    }

    public function createListRows($model, $rowstyle) {
        $str = sprintf("<table>\n", $this->_action, $this->_method);
        $cols = array();
        preg_match_all("/#\{([a-z]+)\}/i", $rowstyle, $cols);
        foreach ($model->data() as $row) {
            $str .= "<tr>";
            $r = $rowstyle;
            for ($i=0; $i<count($cols[0]);++$i) {
                $r = str_replace($cols[0][$i], $row->$cols[1][$i], $r);
            }
            $str .= $r;
            $str .= "</tr>";
        }
        $str .= "</table>";
        return $str;
    }
}

abstract class WidgetInterface {
    static private $_messages = array();
    private $_model = null;

    static public function addMessage($text) {
        self::$_messages[] = new Message($text);
    }

    static public function addError($text) {
        self::$_messages[] = new Message($text, Message::ERROR);
    }

    static public function messages() {
        return self::$_messages;
    }

    final public function setModel($model) {
        if (!$model instanceof AOOSModel) {
            return false;
        }
        $this->_model = $model;
        return true;
    }

    final public function model() {
        return $this->_model;
    }

    final public function __toString() {
        $str = "";
        foreach (WidgetInterface::messages() as $message) {
            $str .= $message;
        }
        $str .= $this->strModel($this->model());
        return $str;
    }

    abstract public function strModel($model);
}

class Message {
    const ERROR         = 1;
    const MESSAGE       = 2;
    const VERIFICATION  = 3;

    private $_type = Message::MESSAGE;
    private $_text = "";
    private $_style = null;

    public function __construct($text, $type = Message::MESSAGE) {
        $this->_text = $text;
        $this->_type = $type;
    }

    public function __toString() {
        return $this->_text;
    }

    public function setText($string) {
        $this->_text = $string;
    }

    public function setType($type) {
        if (!is_integer($type)) {
            return false;
        }
        $this->_type = $type;
    }
}


class FormWidget extends WidgetInterface {
    const NORMAL = 1; // XXX
    const CHECKBOX = 2; // XXX

    const HORIZONTAL = 1; // XXX
    const VERTICAL = 2;

    const ALIGN_LEFT = 1; // XXX
    const ALIGN_MIDDLE = 2; // XXX
    const ALIGN_RIGHT = 3; // XXX

    private $_method = "POST";
    private $_action = "";
    private $_orientation = FormWidget::VERTICAL;
    private $_fields = array();

    public function setMethod($method) {
        if (!$method == "POST" || $method == "GET") {
            return false;
        }
        $this->_method = $method;
        return true;
    }

    public function setAction($action) {
        $this->_action = $action;
        return true;
    }

    public function setOrientation($orientation) {
        if (!($orientation === TableWidget::HORIZONTAL || $orientation === TableWidget::VERTICAL)) {
            return false;
        }
        $this->_orientation = $orientation;
        return true;
    }

    public function setFields($fields) {
        $this->_fields = $fields;
    }

    public function strModel($model) {
        if (empty($this->_fields)) {
            $this->_fields = $model->columnIndex();
        }
        $str = sprintf('<form action="%s" method="%s"><table>', $this->_action, $this->_method);
        switch ($this->_orientation) {
        case(FormWidget::VERTICAL):
            foreach ($this->_fields as $field) {
                $input = $this->_getInputFieldVert($field, $model->getType($field), $model->getProperties($field), $model->getFlags($field), $model->getConstraints($field));
                $str .= $input;
            }
            $str .= '<tr><td></td><td><input type="submit" value="Submit" /></td></tr>';
            break;
        }
        $str .= "</table></form>";
        return $str;
    }

    private function _getInputFieldVert($name, $type, $props, $flags, $constraints) {
        $val = $this->_getInputValue($type, $props, $constraints);

        // GUI_PRIVATE 
        if ($flags & AOOSMODEL_FLAG_GUI_PRIVATE) {
            return '';//sprintf('<tr><td></td><td><input type="hidden" name="%s" value="%s" /></td></tr>', $name, $val);
        }

        if ($type == AOOSMODEL_TYPE_TEXT) {
            $str = sprintf('<tr><td><strong>%s</strong></td><td><textarea name="%s"></textarea></td>',$name, $name);
            return $str;
        }

        $type = $this->_getInputType($type, $props, $constraints);
        $extra = $this->_getInputExtra($type, $props, $constraints);
        $str = sprintf('<tr><td><strong>%s</strong></td><td><input type="%s" name="%s" value="%s" %s/></td></tr>', $name, $type, $name, $val, $extra);
        return $str;
    }

    private function _getInputValue($type, $props, $constraints) {
        return "";
    }

    private function _getInputType($type, $props, $constraints) {
        if ($props & AOOSMODEL_PROP_HASH) {
            return "password";
        }
        if ($type & AOOSMODEL_TYPE_STRING) {
            return "text";
        }
    }

    private function _getInputExtra($type, $props, $constraints) {
        $str = "";
        if ($props & AOOSMODEL_FLAG_GUI_NOTEDITABLE) {
            $str .= "readonly ";
        }
        return $str;
    }
}

class TableWidget extends WidgetInterface {
    const HORIZONTAL = 1;
    const VERTICAL = 2;

    const NO_HEADER = 1;
    const HEADER = 2;

    private $_header = TableWidget::HEADER;
    private $_maxRows = "auto";
    private $_orientation = TableWidget::VERTICAL;
    private $_fields = array();

    public function setOrientation($orientation) {
        if (!($orientation === TableWidget::HORIZONTAL || $orientation === TableWidget::VERTICAL)) {
            return false;
        }
        $this->_orientation = $orientation;
        return true;
    }

    public function setHeader($header) {
        if (!($header === TableWidget::NO_HEADER || $header === TableWidget::HEADER)) {
            return false;
        }
        $this->_header = $header;
        return true;
    }

    public function maxRows($count) {
        if (!(is_integer($count) || strtolower($count) == "auto")) {
            return false;
        }
        $this->_maxRows = is_integer($count) ? $count : strtolower($count);
        return true;
    }

    public function strModel($model) {
        if (empty($this->_fields)) {
            $this->_fields == $model->columnIndex();
        }
        $str = "<table>";
        $rows = $this->_maxRows;
        if ($rows == "auto") {
            $rows = $model->rows();
        }
        switch($this->_orientation) {
        case(TableWidget::HORIZONTAL):
            for ($i=0;$i<$rows;$i++) {
                $str .= "<tr>";
                if ($i==0 && $this->_header == TableWidget::HEADER) {
                    foreach($this->_fields as $index) {
                        $str .= sprintf("<td><strong>%s</strong></td>", $index);
                    }
                    $str .= "</tr><tr>";
                }
                $row = $model->getRow($i)->toArray();
                foreach ($row as $column) {
                    $str .= sprintf("<td>%s</td>", $column);
                }
                $str .= "</tr>";
            }
            break;
        case(TableWidget::VERTICAL):
            foreach ($this->_fields as $index) {
                $str .= "<tr>";
                if ($this->_header == TableWidget::HEADER) {
                    $str .= sprintf("<td><strong>%s</strong></td>", $index);
                }
                foreach ($model->data() as $row) {
                    $str .= sprintf("<td>%s</td>", $row->$index);
                }
            }
            break;
        }

        $str .= "</table>";
        return $str;
    }
}

class ListWidget extends WidgetInterface {
    private $_style = "";
    private $_format = "H:i - d/m/Y";
    private $_pattern = "/#\{([a-zA-Z|]+)\}/";

    public function setStyle($str) {
        if (!is_string($str)) {
            return false;
        }
        $this->_style = $str;
    }

    public function setDateFormat($format) {
        if (!is_string($format)) {
            return false;
        }
        $this->_format = $format;
    }

    public function strModel($model) {
        $str = "";
        $rows = $model->getRows(0, $model->rows()); // get all rows
        foreach ($rows as $row) {
            $str .= $this->_replaceTags($row); // replace #{TAG}s and append to $str
        }
        return $str;
    }

    public function strRow($row) {
        return $this->_replaceTags($row);
    }

    public function strArray($string) {
        return null;
    }

    private function _replaceTags($row) {
        $tags = array();
        $str = $this->_style;
        preg_match_all($this->_pattern, $this->_style, $tags, PREG_SET_ORDER); // find all tags in the current style
        foreach ($tags as $tag) {
            // If "|" is in the tag we need to parse it. Otherwise get it directly from the row
            if (strpos($tag[0], "|") !== false) {
                $value = $this->_parse($tag[0], $row);
            } else {
                $value = $row->$tag[1];
            }
            $str = str_replace($tag[0], $value, $str);
        }
        return $str;
    }

    private function _parse($tag, $row) {
        $tag = preg_replace($this->_pattern, '$1', $tag);
        $parts = explode("|", $tag);
        switch ($parts[0]) {
        case("DATE"):
            return date($this->_format, $row->$parts[1]); // Return the date formatted with $this->_format
            break;
        case("MODULE"):
            return $row->core()->getModule($parts[1])->show(); // Return the result from show() of the module
            break;
        case("URL"):
            switch ($parts[1]) {
            case("current"):
                return $row->core()->getModule("Paginator")->currentURL();
                break;
            case("previous"):
                return $row->core()->getModule("Paginator")->previousURL();
                break;
            case("module"):
                return $row->core()->getModule("Paginator")->createURL($parts[2]);
            }
            break;
        }
    }
}
?>
