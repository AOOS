<?php

class Paginator extends AOOSModule
{
    private $_module = null;
    private $_options = null;
    private $_history = array();

    private function start() {
        // First we clear options from previous
        $this->_options = null;

        $url = "http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
        $url = str_replace($this->core()->getSetting("server_name", __class__), "", $url);
        $parts = explode("/", $url);
        if (strlen($parts[0]) > 0)
        {
            $this->_module = array_shift($parts);
            $this->_options = array();
            foreach ($parts as $f) {
                $p = array_shift($parts);
                if ($p == "") {
                    return false;
                }
                $this->_options[] = $p;
            }
        }
        else {
            $this->_module = $this->core()->getSetting("default_module", __class__);
        }
    }

    public function show()
    {
        $this->start();
        if (!($m = $this->core()->getModule($this->module()))) {
            return false;
        }
        if (sizeof($this->_history) == 0) {
            array_push($this->_history, $this->currentURL());
        }
        elseif ($this->_history[sizeof($this->_history)-1] != $this->currentURL()) {
            array_push($this->_history, $this->currentURL());
        }
        return $m->show();
    }

    public function options() {
        $this->start();
        return $this->_options;
    }

    public function getOption($nr) {
        $opts = $this->options();
        if ($nr >= count($opts)) {
            return false;
        }
        return $opts[$nr];
    }

    public function module() {
        $this->start();
        return $this->_module;
    }

    public function createURL($module, $options = array()) {
        $this->start();
        if (!is_array($options)) {
            throw new AOOSException($this->core(), $this->core()->tr("not_array"), "", true, 1);
            return false;
        }
        $url = $this->core()->getSetting("server_name", __class__);
        $url .= $module."/";
        $opt = implode("/", $options);
        $url .= $opt;
        return $url;
    }

    public function currentURL($short = false) {
        $this->start();
        if ($short) {
            return $this->options() ? $this->module()."/".implode("/",$this->options()) : $this->module();
        }
        else {
            $url = "http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
            if (substr($url, -1) == "/") {
                $url = substr($url,0,-1);
            }
            if (strpos($url, $this->module()) === false) {
                return $url.$this->module();
            }
            return $url;
        }
    }

    public function previousURL() {
        if (sizeof($this->_history) > 1) {
            return $this->_history[sizeof($this->_history)-2];
        } else {
            return $this->currentURL();
        }
    }


    public function numOptions() {
        return count($this->options());
    }

    public function dataModelDefinition() {
        return 0;
    }
}
// vim: number
?>
