<?php

/**
 * An installer-module
 * This module installs a chosen module using the data model definitions of the module. At the moment it is assumed that 
 * all dependencies is installed when trying to install a module.
 * @author Sebastian Skejø
 */

class Installer extends AOOSModule {
    public function show() {
        $url = "http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
        $str = "";

        if (isset($_POST["install"])) {
            if ($this->installModule($_POST["install"])) {
                $str .= '<p style="color: green;">'.$_POST["install"].' installed successfully!</p>';
            }
        }
        elseif (isset($_POST["uninstall"])) {
            if ($this->uninstallModule($_POST["uninstall"])) {
                $str .= '<p style="color: green;">'.$_POST["uninstall"].' uninstalled successfully!</p>';
            }
            else {
                $str .= '<p style="color: red;">'.$_POST["uninstall"].' wasn\'t uninstalled!</p>';
            }
        }

        $str .= "<h1>Installed modules:</h1>";
        foreach ($this->getInstalledModules() as $module) {
            $str .= '<form method="post" action="'.$url.'">
                <p>'.$module.'
                <input name="uninstall" type="hidden" value="'.$module.'" />
                <input type="submit" value="Uninstall" />
                </p>
                </form>';
        }
        $str .= "<h1>Uninstalled modules:</h1>";
        foreach ($this->getUninstalledModules() as $module) {
            $str .= '<form method="post" action="'.$url.'">
                <p>'.$module.'
                <input name="install" type="hidden" value="'.$module.'" />
                <input type="submit" value="Install!" />
                </p>
                </form>';
        }
        return $str;
    }

    public function dataModelDefinition() {
        $m = $this->core()->modules();
        return $m;
    }

    /**
     * Returns an array with names of all uninstalled modules
     * @return array
     */
    public function getUninstalledModules() {
        $all = $this->getAllModules();
        $installed = $this->getInstalledModules();
        $uninstalled = array();

        foreach ($all as $module) {
            if (!in_array($module, $installed)) {
                $uninstalled[] = $module;
            }
        }
        return $uninstalled;
    }

    /**
     * Returns an array with names of all installed modules. 
     * @return array
     */
    public function getInstalledModules() {
        $modules = $this->getAllModules();
        $installed = array();
        foreach ($modules as $module) {
            if ($this->dataModel()->find(array("NAME" => $module)) !== false) {
                $installed[] = $module;
            }
        }
        return $installed;
    }

    /**
     * Returns an array containing names of both installed and uninstalled modules
     * @return array
     */
    public function getAllModules() {
        $d = dir($this->core()->getSetting("module_dir"));
        $modules = array();
        while (false !== ($entry = $d->read())) {
            $path = $this->core()->getSetting("module_dir").$entry;
            if (is_dir($path) && $entry != "." && $entry != "..") {
                $modules[] = $entry;
            }
        }
        return $modules;
    }

    /**
     * Tries to install $module. If install is successfull true is returned, otherwise an AOOSException is thrown and 
     * false is returned.
     * @param $module A string representing the module name
     * @return bool
     */
    public function installModule($module) {
        $path = $this->core()->getSetting("module_dir").$module."/".$module.$this->core()->getSetting("extension");
        require $path;

        $deps = call_user_func(array($module,"dependencies"));
        $deps = implode(",",$deps);
        $this->dataModel()->appendRow(array("NAME" => $module,
            "DEPS" => $deps));

        $m = $this->core()->getModule($module);
        foreach ($m->dataModels() as $dm) {
            $dm->create();
        }

        return true;
    }

    /**
     * Removes a module from the database. If the module is successfully removed true is returned. Otherwise false is 
     * returned. If $module isn't a module in the database an AOOSException is thrown.
     * @param $module A string representing the module name
     * @return bool
     */
    public function uninstallModule($module) {
        $m = $this->core()->getModule($module);
        $m->dataModel()->drop();
        $this->dataModel()->remove(array("NAME" => $module));
        return $this->dataModel()->save();
    }
}
?>
