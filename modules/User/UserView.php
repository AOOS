<?php

class UserView extends AOOSModule {
    static public function dependencies() {
        return array("Paginator", "Form", "Reciever");
    }

    public function add() {
        $f = $this->core()->getModule("Form")->getWidget("Form");
        $r = $this->core()->getModule("Reciever");
        if ($row = $r->getRow("POST")) {
            $this->parent()->createUser($row);
        }
        $f->setFields(array("username", "password", "email"));
        $f->setModel($this->parent()->dataModel("USER"));
        return $f;
    }

    public function edit() {
        if (!$this->parent()->online()) {
            return $this->show();
        }
        $f = $this->core()->getModule("Form")->getWidget("Form");
        $r = $this->core()->getModule("Reciever");
        if ($row = $r->getRow("POST")) {
            $this->parent()->updateUser($row);
        }
        $f->setModel($this->parent()->dataModel("USER"));
        return $f;
    }

    public function show() {
        $r = $this->core()->getModule("Reciever");
        if ($row = $r->getRow("POST")) {
            $this->parent()->login($row);
        }
        if (!$this->parent()->online()) {
            $f = $this->core()->getModule("Form")->getWidget("Form");
            $f->setFields(array("username", "password"));
            $f->setModel($this->parent()->dataModel("USER"));
            return $f;
        }
        return "Hi ".$this->parent()->userInfo()->username;
    }
}
