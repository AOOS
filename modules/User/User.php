<?php

/**
 * User-module
 */

class User extends AOOSModule {
    static public function dependencies() {
        return array("Paginator", "Form", "Reciever");
    }

    static public function files() {
        return array(
            "UserView" => "UserView.php"
        );
    }

    /**
     * Model m1 contains information on the current user. If the user isn't logged in, the model is empty(maybe some 
     * default values later on). When the user logs in, data is fetched from the database and the model contains a 
     * single row with all of the user's information.
     * Model m1 is also used when a new user is created. When that happens, data is inserted into a new row in the model 
     * and save is called on the model.
     * Model m2 contains the uid and the username of all users in the database. This ensures that usernames can be 
     * pulled quickly when you have the uid of the user.
     */
    public function dataModelDefinition() {
        // Current user model
        $m1 = new AOOSModel($this->core());
        $m1->setSource("mysql");
        $m1->setTable("User");
        $m1->setColumnIndex(array("uid", "username", "password", "email", "signature", /*"avatar", */"active", "activationcode", "joined", "status", "groups", "level"));

        $m1->setProperties("uid", AOOSMODEL_TYPE_INTEGER, AOOSMODEL_FLAG_FROM_DATABASE|AOOSMODEL_FLAG_PRIMARY_KEY|AOOSMODEL_FLAG_GUI_PRIVATE);
        
        $m1->setProperties("username", AOOSMODEL_TYPE_STRING, AOOSMODEL_FLAG_FROM_DATABASE, AOOSMODEL_PROP_ESCAPE|AOOSMODEL_PROP_NOHTML|AOOSMODEL_PROP_STRIP);
        $m1->addConstraint("username", "Match", "/[a-zA-Z0-9-_]+/");
        $m1->addConstraint("username", "Length", 50);
        
        $m1->setProperties("password", AOOSMODEL_TYPE_STRING, AOOSMODEL_FLAG_FROM_DATABASE, AOOSMODEL_PROP_HASH|AOOSMODEL_PROP_STRIP);
        
        $m1->setProperties("email", AOOSMODEL_TYPE_STRING, AOOSMODEL_FLAG_FROM_DATABASE, AOOSMODEL_PROP_ESCAPE|AOOSMODEL_PROP_NOHTML|AOOSMODEL_PROP_STRIP);
        $m1->addConstraint("email", "Match", "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/"); // Match email
        
        $m1->setProperties("signature", AOOSMODEL_TYPE_TEXT, AOOSMODEL_FLAG_FROM_DATABASE, AOOSMODEL_PROP_ESCAPE|AOOSMODEL_PROP_NOHTML);
        $m1->addConstraint("signature", "Length", 256);

        // XXX        $m1->setProperties("avatar", AOOSMODEL_TYPE_STRING, AOOSMODEL_FLAG_FROM_DATABASE, AOOSMODEL_PROP_ESCAPE); 
        
        $m1->setProperties("active", AOOSMODEL_TYPE_BOOLEAN, AOOSMODEL_FLAG_FROM_DATABASE|AOOSMODEL_FLAG_GUI_PRIVATE);
        $m1->setDefaultValue("active", false);

        $m1->setProperties("activationcode", AOOSMODEL_TYPE_INTEGER, AOOSMODEL_FLAG_FROM_DATABASE|AOOSMODEL_FLAG_GUI_PRIVATE);

        $m1->setProperties("joined", AOOSMODEL_TYPE_INTEGER, AOOSMODEL_FLAG_FROM_DATABASE|AOOSMODEL_FLAG_GUI_PRIVATE, AOOSMODEL_PROP_TIME);

        $m1->setProperties("status", AOOSMODEL_TYPE_INTEGER,AOOSMODEL_FLAG_GUI_PRIVATE);
        $m1->setDefaultValue("status",0);// $this->getStatusValue("offline"));
        
        $m1->setProperties("groups", AOOSMODEL_TYPE_TEXT, AOOSMODEL_FLAG_FROM_DATABASE|AOOSMODEL_FLAG_GUI_PRIVATE);
        
        $m1->setProperties("level", AOOSMODEL_TYPE_INTEGER, AOOSMODEL_FLAG_FROM_DATABASE|AOOSMODEL_FLAG_GUI_PRIVATE);
        
        $m1->appendRow();
        $m1->getRow(0)->setFlag(0);

        // All users. Only the most important fields are fetched here
        $m2 = new AOOSModel($this->core());
        $m2->setSource("mysql");
        $m2->setTable("User");
        $m2->setColumnIndex(array("uid", "username", "active", "activationcode"));
        $m2->setProperties("uid", AOOSMODEL_TYPE_INTEGER, AOOSMODEL_FLAG_FROM_DATABASE|AOOSMODEL_FLAG_PRIMARY_KEY);
        $m2->setProperties("username", AOOSMODEL_TYPE_STRING, AOOSMODEL_FLAG_FROM_DATABASE|AOOSMODEL_FLAG_UNIQUE, AOOSMODEL_PROP_ESCAPE|AOOSMODEL_PROP_NOHTML|AOOSMODEL_PROP_STRIP);
        $m2->setProperties("active", AOOSMODEL_TYPE_BOOLEAN, AOOSMODEL_FLAG_FROM_DATABASE|AOOSMODEL_FLAG_GUI_PRIVATE);
        $m2->setDefaultValue("active", false);
        $m2->setProperties("activationcode", AOOSMODEL_TYPE_INTEGER, AOOSMODEL_FLAG_FROM_DATABASE|AOOSMODEL_FLAG_GUI_PRIVATE);

        $a = array("USER" => $m1, "USERLIST" => $m2);
        return $a;
    }

    public function show() {
        $this->dataModel("USERLIST")->populate();
        require_once("UserView.php");
        $this->_view = new UserView($this);
        $p = $this->core()->getModule("Paginator");
        switch($p->getOption(0)) {
        case("edit"):
            return $this->_view->edit();
            break;
        case("add"):
            return $this->_view->add();
            break;
        case("activate"):
            $this->activateUser($p->getOption(1), $p->getOption(2));
            return $this->_view->show();
            break;
        default:
            return $this->_view->show();
            break;
        }
    }

    /**
     * Creates the user 
     */
    public function createUser($info) {
        if ($this->dataModel("USERLIST")->find(array("username" => $info->username))) {
            return false;
        }
        $a = $info->toArray()+array("activationcode" => time());
        try {
            $this->dataModel("USER")->appendRow($a);
        } catch (AOOSException $e) {
            throw $e;
        }
        // Appending was succesful
        $this->dataModel("USER")->save();
        $this->dataModel("USERLIST")->populate();
        $p = $this->core()->getModule("Paginator");
        $uid = $this->dataModel("USERLIST")->find(array("username" => $info->username))->uid;
        $url = $p->createURL("User", array("activate", $uid, $a["activationcode"]));
        mail($info->email, "Activation code", "Activate at: ".$url);
        return true;
    }

    public function updateUser($row) {
        if (!$this->online()) {
            return false;
        }
        foreach ($row->toArray() as $key => $val) {
            $this->dataModel("USER")->getRow(0)->$key = $val;
        }
        $this->dataModel("USER")->save();
        return true;
    }

    public function activateUser($uid, $code) {
        $r = $this->dataModel("USERLIST")->find(array("uid" => $uid));
        if ($r->activationcode == $code) {
            $r->activationcode = 0;
            $r->active = true;
            $r->save();
        }
        return true;
    }

    public function login($row) {
        $dm = $this->dataModel("USER");
        $w = $row->toArray();
        $w["active"] = true;
        $dm->populate($w);
        if ($dm->rows() == 0) {
            WidgetInterface::addError("login_failed");
            return false;
        }
        $dm->getRow(0)->status = 1;//$this->core()->getSetting("online", __class__);
        return true;
    }

    /* ---- Convinience functions ---- */
    public function status() {
        if ($this->dataModel("USER")->rows() > 0) {
            return $this->dataModel("USER")->getRow(0)->status;
        }
        return 0;
    }

    public function getStatusValue($string) {
        return $this->core()->getSetting("status_".$string, __class__);
    }

    public function online() {
        return $this->status() == 1;//$this->getStatusValue("online");
    }

    public function level() {
        if (!$this->online()) {
            return false;
        }
        return $this->dataModel("USER")->getRow(0)->level;
    }

    public function groups() {
        if (!$this->online()) {
            return false;
        }
        return $this->dataModel("USER")->getRow(0)->groups;
    }

    public function inGroup($group) {
        if (!$this->online()) {
            return false;
        }
        return in_array($group, $this->groups());
    }

    public function userInfo() {
        return $this->dataModel("USER")->getRow(0);
    }
}
?>
