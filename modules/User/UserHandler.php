<?php

/**
 * Handles user data
 * @author Sebastian Skejø
 */

class UserHandler extends AOOSModule
{
    private $_storageObj = null;

    public function __construct($core) {
        parent::__construct($core);

        $this->_storageObj = $this->core()->newStorageDevice();
        $this->_storageObj->setTable("User");
    }
    /**
     * Returns a model with the fields given in $fields, selected by $where and ordered by $sort
     * @param array $fields Fields to be returned
     * @param where-clause $where A valid where-clause
     * @param field $order The field the data is ordered by
     * @param ASC|DESC $sort Determines if data should be ordered 
     * @return AOOSModel
     */
    public function getUserList($fields, $where = null, $limit = null, $order = "username", $sort = "ASC") {
        $this->_storageObj->setSort($sort);
        $model = $this->_storageObj->selectModel($fields, $where, $limit, $order);
        return true;
    }

    /**
     * Tries to login with the given $username and $password
     * @param AOOSModel $data A model containing data to login
     * @return bool
     */
    public function login(AOOSModel $data) {
        // We have to do it in this order since passwords don't need quotes until we it is encrypted
        // XXX This part is quite ugly - should be take care of in StorageDevice
        $password = $data->getColumn("PASSWORD", true);
        $data->setQuote(true);
        $username = $data->getColumn("USERNAME", true);

        $password = hash("sha256", $password); // XXX Need to check if this is supported on the server!
        $password = "'".$password."'";
        $u = $this->core()->getModule("User");
        
        $where = array(
            "USERNAME" => $username,
            "PASSWORD" => $password,
            "ACTIVATED" => 1
        );
        if ($this->_storageObj->numRows($where) == 0) {
            throw new AOOSException($this->core(), $this->tr("login_failed", "User"), $this->tr("check_user_pass_active", "User"), true, 1);
            return false;
        }

        $m = $this->_storageObj->selectModel("*", $where);
        // Set the userdata
        $u->setUsername($m->getColumn("USERNAME", true));
        $u->setPassword($m->getColumn("PASSWORD", true));
        //        $u->setStatus(1); // 1 == online XXX Could be neat
        $u->setLoggedIn(true);
        $u->setEmail($m->getColumn("EMAIL", true));
        $u->setGroups($m->getColumn("GROUPS", true));
        $u->setLevel($m->getColumn("LEVEL", true));
        return true;
    }

    /**
     * Creates a user in the database
     * @param AOOSModel $data The model containing all the data for the user to be created
     * @return bool
     */
    public function createUser($data) {
        $username = $data->getColumn("USERNAME", true);
        $where = array("USERNAME" => "'".$username."'");
        if ($this->_storageObj->numRows($where) != 0) {
            throw new AOOSException($this->core(), $this->tr("username_taken", "User"), "", true, 1);
            return false;
        }

        // We only want to insert these fields
        $fields = array(
            "USERNAME",
            "PASSWORD",
            "EMAIL"
        );

        // Encryption of password
        $uPass = $data->getColumn("PASSWORD", true);
        $pass = hash("sha256", $uPass);
        $data->setData($pass, -1, "PASSWORD");

        $data->setColumnIndex($fields);
        $data->setQuote(true);
        if ($this->_storageObj->insertModel($data)) {
            // XXX Send email
            return true;
        }
        return false;
    }

    /**
     * Activates a given user
     * @param AOOSModel $data A model containing username and password
     * @return bool
     */
    public function activateUser($data) {
        $username = $data->getColumn("USERNAME", true);
        $password = hash("sha256", $data->getColumn("PASSWORD", true));
        $data->setData($password, -1, "PASSWORD");

        $where = array(
            "USERNAME" => "'".$username."'",
            "PASSWORD" => "'".$password."'",
            "ACTIVATED" => 0
        );
        $fields = array_keys($where);
        $data->setColumnIndex($fields);
        print_r($where);

        if ($this->_storageObj->numRows($where) == 0) {
            throw new AOOSException($this->core(), $this->tr("user_not_activatable", "User"), "", true, 1);
            return false;
        }

        $data->setData(1, -1, "ACTIVATED");
        $data->setQuote(true);
        return $this->_storageObj->updateFromModel($data, $where);
    }

    /**
     * Deletes the user, $username
     * @param string $username The user
     * @return bool
     */
    public function deleteUser($username) {
        // Only admins have rights to do this
        if (!$this->core()->getModule("user")->checkLevel("admin")) {
            throw new AOOSException($this->core(), $this->tr("access_denied", "User"), "", true, 1);
            return false;
        }

        $d = array("USERNAME" => $username);
        return $this->_storageObj->deleteFromArray($d);
    }

    /**
     * Updates the data for the given user
     * @param string $username The username of the user which data we update
     * @param AOOSModel $data All the new data, saved in an AOOSModel
     * @return bool
     */
    public function updateUser($username, $data) {
        $u = $this->core()->getModule("user");

        // Only admins or the user itself have rights to do this
        if (!($u->checkLevel("admin") || $u->username() == $username)) {
            throw new AOOSException($this->core(), $this->tr("access_denied", "User"), "", true, 1);
            return false;
        }

        $where = array("USERNAME" => $username);
        return $this->_storageObj->updateFromModel($data, $where);
    }
}
