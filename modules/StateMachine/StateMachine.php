<?php

/**
 * A state machine framework for AOOS.
 * @author Sebastian Skejø
 */
class StateMachine extends AOOSModule {
    /**
     * setState("User/login", "POST/USERNAME/PASSWORD", "login", array($_POST["USERNAME"], $_POST["PASSWORD"]))
     */
    public function setState($page, $ifset, $callback, $args) {
        $this->_pages->appendRow(array(
            "PAGE" => $page,
            "IFSET" => $ifset,
            "CALLBACK" => $callback,
            "ARGS" => $args));
    }

    public function dataModelDefinition() {
        $pages = new AOOSModel($this->core());
        $pages->setColumnIndex(array("PAGE", "IFSET", "CALLBACK", "ARGS"));
        $pages->setProperty("PAGE", AOOSMODEL_TYPE_STRING, 0);
        $pages->setProperty("IFSET", AOOSMODEL_TYPE_STRING, 0);
        $pages->setProperty("CALLBACK", AOOSMODEL_TYPE_STRING, 0);
        $pages->setProperty("ARGS", AOOSMODEL_TYPE_ARRAY, 0);
        return $pages;
    }

    public static function dependencies() {
        return array("Paginator");
    }

    public function execute() {
        $url = $this->core()->getModule("Paginator")->currentURL(true);
        $states = $this->dataModel()->findAll(array("PAGE"=>$url));
        foreach ($states as $state) {
            $ifset = explode("/", $state["IFSET"]);
            $type = array_shift($ifset);
            if ($type == "POST") {
                $d = $_POST;
            }
            elseif ($type == "GET") {
                $d = $_GET;
            }
            foreach ($ifset as $ifsetname) {
                if (!isset($d[$ifsetname])) {
                    continue 2;
                }
            }
            $mname = array_shift(explode("/", $state["PAGE"]));
            $m = $this->core()->getModule($mname);
            return call_user_func_array(array($m, $state["CALLBACK"]), $state["ARGS"]);
        }
        return false;
    }
}
?>
