<?php 
session_start();
if (!isset($_SESSION["files"])) {
    $_SESSION["files"] = array();
}
include("tmp/header.php");
//include("tmp/translatebox.php");

require_once("AOOSCore.php");
require_once("AOOSModule.php");
require_once("AOOSModel.php");
//require_once("AOOSStorageDevice.php");

function __autoload($class) {
    $p = false;
    foreach ($_SESSION["files"] as $path => $c) {
        if ($c == $class) {
            $p = $path;
        }
    }
    if (!$p) {
        exit("Couldn't find definition of $class");
    }
    require_once($p);
}

try {
    global $c;
//    unset($_SESSION); //FOR DEBUGGING
    if (!isset($_SESSION["core"])) {
        $c = new AOOSCore();
        $_SESSION["core"] = serialize($c);
    }
    $c = unserialize($_SESSION["core"]);


    $p = $c->getModule("Paginator");
    $f = $c->getModule("Form");
    print $p->show();


    print "<p>".$c->log2str()."</p>";
    $_SESSION["core"] = serialize($c);
} catch (AOOSException $e) {
    print $e;
}

$c->printExceptions();

?>
