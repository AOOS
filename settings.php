<?php
$settings["DBUsername"]     = "root";
$settings["DBPassword"]     = "";
$settings["DBHost"]         = "localhost";
$settings["DBDatabase"]     = "AOOS";
$settings["DBPrefix"]       = "AOOS_";

$settings["lang"]           = "en";
$settings["module_dir"]     = "modules/";
$settings["module_path"]    = "http://localhost/~sebastian/AOOS/".$settings["module_dir"];
$settings["module_table"]   = "Modules";
$settings["extension"]      = ".php";

$settings["default_storage_type"]   = "mysql";
?>
