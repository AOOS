<?php

/**
 * AOOSModule
 *
 * Base module for all modules
 *
 * @author Sebastian Skejø
 */
class AOOSModule
{
    private $_core      = null;
    private $_parent    = null;
    private $_name      = null;
    private $_data      = array();
    private $_slots     = array();

    /**
     * Subclasses which overrides a constructor *must* take an AOOSCore or an AOOSModule as first argument and call 
     * parent::__construct(core) in order to work properly
     * @param $parent A reference to the parent object
     */
    public function __construct($parent) {
        $this->_init($parent);
    }

    /**
     * Internal function used by constructor to set things up
     * @param $core A reference to the core object
     */
    private function _init($parent) {
        if ($parent instanceof AOOSModule) {
            $this->_core = $parent->core();
            $this->_parent = $parent;
        }
        elseif ($parent instanceof AOOSCore) {
            $this->_core = $parent;
        }
        else {
            return false;
        }

        $dbg = debug_backtrace();
        $str = "AOOSModule instantiated in ";
        $dbg = array_slice($dbg, 1,sizeof($dbg));
        foreach ($dbg as $entry) {
            $file = in_array("file", array_keys($entry)) ? $entry["file"] : "unknown file";
            $line = in_array("line", array_keys($entry)) ? $entry["line"] : "unknown line";
            $str .= $file.":".$line."<br />";
        }
        $this->core()->log($str);
    }

    public function __toString() {
        return __class__;
    }

    /**
     * This function is executed after object is constructed an data model is set
     */
    public function postInitialization() {
        return true;
    }

    /**
     * Returns an array of dependencies or 0. If your module depends on any other module you should override this 
     * function so it returns an array of the names of the module on which your module depends.
     * @return array
     */
    public static function dependencies() {
        return array();
    }

    /**
     * Returns an associative array of classes used in the module as keys and the path to the file in which they are 
     * defined as values. Paths are relative to the module diretory.
     * Example:
     * Module Foo uses the class Bar. Bar is defined in modules/Foo/BarClass.php. Foo::files() should be defined as:
     * \code
     * class Foo {
     *     ...
     *     static public function files() {
     *        return array("Bar" => "BarClass");
     *     }
     *     ...
     * }
     * \code
     * @return array
     */
    public static function files() {
        return array();
    }

    /**
     * Standard show-function.
     * Returns the name of the module
     */
    public function show() {
        return $this->name();
    }

    /**
     * Translates a given string to language selected in /settings.php. Throws an AOOSLangException if the translated isn't 
     * found.
     * @param $stringID The unique ID of the string to translate
     * @return string
     */
    final public function tr($stringID) {
        $string = $this->core()->getTranslatedString($stringID);
        return $string;
    }

    /**
     * Returns the value of setting $setting. If the module is named, it will look in the module settings, otherwise it 
     * will look in the global settings. Throws an AOOSException if the setting isn't found.
     * @param $setting Name of the setting
     * @return string|false
     */
    final public function getSetting($setting) {
        try {
            if ($this->name()) {
                $settingValue = $this->core()->getSetting($setting, $this->name());
            }
            else {
                $settingValue = $this->core()->getSetting($setting);
            }
        }
        catch (AOOSException $e) {
            throw $e;
            return false;
        }
        return $settingValue;
    }


    /**
     * Returns the current core
     * @return AOOSCore
     */
    final public function core() {
        return $this->_core;
    }

    /**
     * Returns the parent object or null if no parent object is set
     * @return AOOSModule|null
     */
    final public function parent() {
        return $this->_parent;
    }

    /**
     * Sets the name of the module. This is automatically called when a module is instantiated.
     * @param $name Module name
     * @sa name
     * @return true
     */
    final public function setName($name) {
        $this->_name = $name;
        return true;
    }

    /**
     * Returns the name of the module
     * @sa setName
     * @return string
     */
    final public function name() {
        if ($this->_name) {
            return $this->_name;
        }
        return "Unnamed module";
    }

    /**
     * This function must defined to either return a proper AOOSModel or 0. This model must be a representation of all 
     * the data that the module should contain.
     * Please note that the model shouldn't be populated or have table or source set in this function. This should be 
     * done in AOOSModule::postInitialization().
     * @sa setDataModel, dataModel
     */
    public function dataModelDefinition() { 
        return 0;
    }

    /**
     * Sets the data model to be $model
     * @param $model The model to use as data model. Optionally this can be an array containing models.
     * @sa dataModelDefinition, dataModel
     * @return true
     */
    final public function setDataModel($models) {
        $this->_data = array(); // XXX Not sure about this
        if (is_array($models)) {
            foreach ($models as $name => $model) {
                if ($model instanceof AOOSModel) {
                    $this->_data[$name] = $model;
                }
            }
        }
        elseif ($models instanceof AOOSModel || $models == null) {
            $this->_data[] = $models;
        }
    }

    /**
     * Returns the set data model
     * @param $name An optional parameter to decide which data model to return if more than one data model is defined. If 
     * this parameter isn't set, the first data model set is returned. If $name doesn't match the name of any data model 
     * an AOOSException is thrown.
     * @sa dataModelDefinition, setDataModel
     * @return AOOSModel
     */
    final public function dataModel($name = null) {
        if (empty($this->_data)) {
            return false;
        }
        if ($name == null) {
            return $this->_data[0];
        } else {
            if (in_array($name, array_keys($this->_data))) {
                return $this->_data[$name];
            } else {
                throw new AOOSException($this->core(), $this->tr("data_model_not_found"));
            }
        }
    }

    public function dataModels() {
        return $this->_data;
    }

    /**
     * Emits the signal $signal.
     * This means that every slot connected to this function is called.
     * @param $signal A string matching the signal name.
     * @param $params An optional array of parameters to pass to the connected slot(s)
     */
    final public function emit($signal, $params = null) {
        if (!in_array($signal, array_keys($this->_slots))) {
            return false;
        }
        foreach ($this->_slots[$signal] as $slot) {
            if (!is_array($params) || $slot[2] == 0) {
                return call_user_func(array($slot[0], $slot[1]));
            }

            if (sizeof($params) > $slot[2]) {
                $params = array_slice($params, 0, $slot[2]);
            }
            elseif (sizeof($params) < $slot[2]) {
                throw new AOOSException($this->core(), $this->tr("not_enough_params"));
                return false;
            }
            return call_user_func_array(array($slot[0], $slot[1]), $params);
        }
        return true;
    }

    /**
     * Connects the signal $signal of $this module to the slot, $slot of the the $other module.
     * @param $signal A string matching the signal name.
     * @param $other The module in which the slot is.
     * @param $slot A string matching the slot name.
     * @param $numargs The number of arguments the $slot takes. Defaults to 0
     * @return true
     */
    final public function connect($signal, $other, $slot, $numargs = 0) {
        if (!in_array($signal, array_keys($this->_slots))) {
            $this->_slots[$signal] = array();
        }
        $a = array($other, $slot, $numargs);
        $this->_slots[$signal][] = $a;
        return true;
    }
}
// vim: number
?>
