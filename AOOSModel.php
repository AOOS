<?php
AOOSCore::register("AOOSMysqlInterface", "lib/AOOSMysqlInterface.php");
require_once("lib/AOOSModelConstraints.php");

define('AOOSMODEL_TYPE_STRING',   1);
define('AOOSMODEL_TYPE_TEXT',     2);
define('AOOSMODEL_TYPE_INTEGER',  3);
define('AOOSMODEL_TYPE_BOOLEAN',  4);
define('AOOSMODEL_TYPE_UNKNOWN',  5);

define('AOOSMODEL_PROP_HASH',       1);
define('AOOSMODEL_PROP_NOHTML',     2);
define('AOOSMODEL_PROP_STRIP',      4);
define('AOOSMODEL_PROP_ESCAPE',     8);
define('AOOSMODEL_PROP_TIME',       16);

define('AOOSMODEL_FLAG_FROM_DATABASE',  1);
define('AOOSMODEL_FLAG_UNIQUE',         2);
define('AOOSMODEL_FLAG_PRIMARY_KEY',    4);
define('AOOSMODEL_FLAG_GUI_PRIVATE',    8);
define('AOOSMODEL_FLAG_GUI_NOTEDITABLE',16);

define('AOOSMODELROW_PRIVATE_INSERT',     1);
define('AOOSMODELROW_PRIVATE_UPDATE',     2);
define('AOOSMODELROW_PRIVATE_REMOVE',     3);

define('AOOSMODEL_CACHE_STATIC',    1); // Only populate if empty
define('AOOSMODEL_CACHE_DYNAMIC',   2); // Clear and populate always

class AOOSModel extends AOOSModule {
    private $_data          = array();
    private $_columnIndex   = array();
    private $_properties    = array();
    private $_constraints   = array();
    private $_types         = array();
    private $_flags         = array();
    private $_defaults      = array();
    private $_storage       = null;
    private $_rows          = 0;
    private $_lastMatch     = 0;
    private $_cache         = AOOSMODEL_CACHE_DYNAMIC;

    private $_table         = null;

    /* ------ Data functions ------ */
    /**
     * Appends a row to the end of the model. If given an array, the function creates the row with the values specified 
     * in the array. Otherwise the row is empty.
     * @param $values An array with the values to fill the row with. Array keys are column indeces and array values are 
     * the values used to fill the array.
     * @return true
     */
    public function appendRow($values = null) {
        if ($values instanceof AOOSModelRow) {
            $values = $values->toArray();
        }
        $row = new AOOSModelRow($this, $values, AOOSMODELROW_PRIVATE_INSERT);
        $this->_data[] = $row;
        $this->_rows++;
        return true;
    }

    /**
     * Sets value of $column in row $row to be $value. Throws an AOOSException if $row isn't valid, if the specified 
     * column isn't a valid column index or setting the value for some reason failed.
     * @param $row An integer representing the row
     * @param $column A valid column index.
     * @param $value The value
     * @return boolean
     */
    public function setData($row, $column, $value) {
        try {
            $r = $this->getRow($row);
            $ret = $r->$column = $value;
        } catch (AOOSException $e) {
            throw $e;
        }
        return $ret;
    }

    /**
     * Returns all rows that haven't been removed
     * @return array
     */
    public function data() {
        $rows = array();
        foreach ($this->_data as $row) {
            if ($row->flag() != AOOSMODELROW_PRIVATE_REMOVE) {
                $rows[] = $row;
            }
        }
        return $rows;
    }

    /**
     * Returns the row at $index position. If a negative value is given it is counted from the end. Throws an AOOSException 
     * if $index isn't within bounds.
     * @param $index An integer representing the row
     * @sa getRows
     * @return AOOSModelRow
     */
    public function getRow($index) {
        if ($index < 0) {
            $index = $this->rows()+$index;
        }
        if($index >= $this->rows() || $index < 0) {
            throw new AOOSException($this->core(), $this->tr("bound_error"), $index);
        }
        return $this->_data[$index];
    }

    /**
     * Returns all rows from $start until $length rows is found. If $start is negative, it is counted from the end. Throws an AOOSException if $start or $start+$length aren't within bounds.
     * @param $start The start row. An integer.
     * @param $length Get $length number of rows. An integer.
     * @sa getRows
     * @return array
     */
    public function getRows($start, $length) {
        $rows = array();
        if ($start < 0) {
            $start = $this->rows()+$start;
        }
        elseif ($start > $this->rows() || $start+$length > $this->rows()) {
            throw new AOOSException($this->core(), $this->tr("bound_error"));
        }
        for ($i=0; $i<$length; ++$i) {
            $rows[$start] = $this->_data[$start];
            $start++;
        }
        return $rows;
    }

    /**
     * Returns the number of rows.
     * @return integer
     */
    public function rows() {
        return $this->_rows;
    }

    /**
     * Removes rows that match $where. $limit defines the number of rows to remove. Both $where and $limit are arrays. 
     * $limit consists of two entries, $limit[0] which defines the start of which rows to remove and $limit[1] which 
     * defines the number of rows to remove. Throws an AOOSException if $where or $limit aren't arrays.
     * @param $where An array against which all rows are matched.
     * @param $limit The limit array.
     * @return true.
     */
    public function remove($where, $limit) {
        if (!is_array($where) || !is_array($limit)) {
            throw new AOOSException($this->core(), $this->tr("not_array"));
        }
        $rows = $this->findAll($where);
        $rows = array_slice($rows, $limit[0], $limit[1]);
        foreach ($rows as $row) {
            $row->setFlag(AOOSMODELROW_PRIVATE_REMOVE);
        }
        return true;
    }

    /**
     * Returns the column $column. $start defines the start row and $length defines the number of rows. $start and 
     * $length are optional. Throws an AOOSException if $column isn't a valid column index.
     * @param $column The column name.
     * @param $start An integer representing the start row. Defaults to 0.
     * @param $length The number or rows to use. Defaults to all rows.
     * @return array
     */
    public function getColumn($column, $start = 0, $length = null) {
        $columnA = array();
        if (!$this->inColumnIndex($column)) {
            throw new AOOSException($this->core(), $this->tr("index_error"));
        }
        if ($length === null) {
            $length = $this->rows();
        }
        $rows = $this->getRows($start, $length);
        foreach ($rows as $index => $row) {
            $columnA[$index]  = $row[$column];
        }
        return $columnA;
    }

    /**
     * Returns the first row that matches $match from $offset and forwards. The rows are matched using preg_match and 
     * delimiters should NOT be included in the array values. Throws an AOOSException if $offset isn't within bounds.
     * @param $match An array in which keys are column names and values are the values to match against.
     * @param $offset The row to start searching in.
     * @sa findAll
     * @return AOOSModelRow or false if no rows are found.
     */
    public function find($match, $offset = 0) {
        $m = false;
        for($i = $offset; $i < $this->rows(); ++$i) {
            try {
                $row = $this->getRow($i);
                if ($row->match($match) !== false) {
                    $m = $row;
                    $this->_lastMatch = $i;
                    break;
                }
            } catch (AOOSException $e) {
                throw $e;
            }
        }
        return $m;
    }

    /**
     * Returns all rows that match $match from $offset and forwards. Throws an AOOSException if $offset isn't within 
     * bounds.
     * @param $match An array in which keys are column names and values are the values to match against.
     * @param $offset The row to start searching in.
     * @sa find
     * @return array
     */
    public function findAll($match, $offset = 0) {
        $m = array();
        $i = 0;
        while (true) {
            try {
                $row = $this->find($match, $i);
            } catch (AOOSException $e) {
                throw $e;
            }
            if ($row === false) {
                break;
            }
            $m[] = $row;
            $i = $this->_lastMatch+1;
        }
        return $m;
    }

    /**
     * Empties the model and removes all propeties, constraints and flags. Only column indeces are kept.
     * @sa resetData
     * @return true
     */
    public function reset() {
        $this->_data = array();
        $this->_properties = array();
        $this->_constraints = array();
        $this->_flags = array();
        $this->_defaults = array_fill_keys($this->columnIndex(), null);
        $this->_rows = 0;
        return true;
    }

    /**
     * Empties the model.
     * @sa reset
     * @return true;
     */
    public function resetData() {
        $this->_data = array();
        $this->_rows = 0;
        return true;
    }

    /* ----- Cache mode ------ */
    public function setCacheMode($cacheMode) {
        if ($cacheMode != AOOSMODEL_CACHE_STATIC && $cacheMode != AOOSMODEL_CACHE_DYNAMIC) {
            throw new AOOSExcpetion($this->core(), $this->tr("not_valid_cache_mode"));
        }
        $this->_cache = $cacheMode;
        return true;
    }

    public function cacheMode() {
        return $this->_cache;
    }

    
    /* ----- Column index ------ */
    public function setColumnIndex($columnindex) {
        if (!is_array($columnindex)) {
            throw new AOOSException($this->core(), $this->tr("not_array"));
        }
        $this->_columnIndex = $columnindex;
        $this->reset();
        return true;
    }

    public function columnIndex() {
        return $this->_columnIndex;
    }

    public function inColumnIndex($column) {
        return in_array($column, $this->columnIndex());
    }


    /* ------ Properties, types, flags and constraints ------ */
    public function setProperties($column, $type, $flag = 0, $properties = 0) {
        if (!$this->inColumnIndex($column)) {
            throw new AOOSException($this->core(), $this->tr("index_error"));
        }
        if (!is_integer($properties) || !is_integer($type) || !is_integer($flag)) {
            throw new AOOSException($this->core(), $this->tr("not_integer"));
        }
        $this->_properties[$column] = $properties;
        $this->_types[$column]      = $type;
        $this->_flags[$column]      = $flag;
        return null;
    }

    public function getProperties($column) {
        if (!$this->inColumnIndex($column)) {
            throw new AOOSException($this->core(), $this->tr("index_error"));
        }
        if (!in_array($column, array_keys($this->_properties))) {
            return false;
        }
        return $this->_properties[$column];
    }

    public function addConstraint($column, $constraint, $args) { // XXX Variable argument count
        if (!$this->inColumnIndex($column)) {
            throw new AOOSException($this->core(), $this->tr("index_error"));
        }
        $name = $constraint."Constraint";
        if (!class_exists($name)) {
            return false;
        }
        if (!is_array($args)) {
            $args = array($args);
        }
        $numArgs = call_user_func(array($name, "numArgs"));
        if (count($args) != $numArgs) {
            return false;
        }
        if (!in_array($column,array_keys($this->_constraints))) {
            $this->_constraints[$column] = array();
        }
        $this->_constraints[$column][] = array($name, $args);
        return true;
    }

    public function getConstraints($column) {
        if (!$this->inColumnIndex($column)) {
            throw new AOOSException($this->core(), $this->tr("index_error"));
        }
        if (!in_array($column, array_keys($this->_constraints))) {
            return false;
        }
        return $this->_constraints[$column];
    }

    /**
     * Returns the type of $column.
     * @param Column name.
     * @return integer
     */
    public function getType($column) {
        if (!$this->inColumnIndex($column)) {
            throw new AOOSException($this->core(), $this->tr("index_error"));
        }
        return $this->_types[$column];
    }

    public function setFlags($column, $flag) {
        if (!$this->inColumnIndex($column)) {
            throw new AOOSException($this->core(), $this->tr("index_error"));
        }
        if (!is_integer($flag)) {
            throw new AOOSException($this->core(), $this->tr("not_integer"));
        }

        $this->_flags[$column] = $flag;
        return true;
    }

    public function getFlags($column) {
        if (!$this->inColumnIndex($column)) {
            throw new AOOSException($this->core(), $this->tr("index_error"));
        }
        if (!in_array($column, array_keys($this->_flags))) {
            return false;
        }
        return $this->_flags[$column];
    }

    /* ----- Default values ------ */
    /**
     * Sets the $column to have the default value of $value. Throws an AOOSException if column name isn't valid.
     * @param $column Column name
     * @param $value Default value
     * @return true
     */
    public function setDefaultValue($column, $value) {
        if (!$this->inColumnIndex($column)) {
            throw new AOOSExcpetion($this->core(), $this->tr("index_error"));
        }
        $this->_defaults[$column] = $value;
        return true;
    }

    /**
     * Returns the default value of the given column. Throws an AOOSException if column name isn't valid.
     * @param $column Column name.
     * @return value or false if none is set
     */
    public function getDefaultValue($column) {
        if (!$this->inColumnIndex($column)) {
            throw new AOOSException($this->core(), $this->tr("index_error"));
        }
        if (!in_array($column, array_keys($this->_defaults))) {
            return false;
        }
        return $this->_defaults[$column];
    }

    /**
     * Returns the array of default values. Note that if a column isn't given a default value, null is used per default.
     * @return array
     */
    public function defaultValues() {
        return $this->_defaults;
    }


    /* ----- Storage ----- */
    /**
     * Sets the type of the database.
     * This must be called before AOOSModel::setTable().
     * @param $source A string containing the type. Implemented: mysql
     * @return true
     */
    public function setSource($source) {
        if (!is_string($source)) {
            throw new AOOSException($this->core(), $this->tr("not_string"));
        }

        switch (strtolower($source)) {
        case("mysql"):
            require_once("lib/AOOSMysqlInterface.php");
            $this->_storage = new AOOSMysqlInterface($this->core());
            break;
        default:
            exit("Not valid database");
        }
        return true;
    }

    /**
     * Sets the table on which AOOSModel::populate() and AOOSModel::save() operates on.
     * @param $table The table name. Must be a string.
     * @return boolean
     */
    public function setTable($table) {
        if (!is_object($this->_storage)) {
            throw new AOOSException($this->core(), $this->tr("database_not_initialized"));
        }
        if (!is_string($table)) {
            throw new AOOSException($this->core(), $this->tr("not_string"));
        }
        $table = $this->core()->getSetting("DBPrefix").$table;
        $this->_table = $table;

        return $this->_storage->setTable($table);
    }

    public function table() {
        return $this->_table;
    }

    public function populate($where = null, $order = null, $limit = null) {
        switch($this->_cache) {
        case(AOOSMODEL_CACHE_STATIC):
            if ($this->rows() != 0) {
                return false;
            }
            break;
        case(AOOSMODEL_CACHE_DYNAMIC):
            $this->resetData();
            break;
        }
        $fields = array();
        if (is_array($where)) {
            $where = new AOOSModelRow($this, $where);
            $where = $where->toDatabaseArray();
        }
        elseif ($where instanceof AOOSModelRow) {
            $where = $where->toDatabaseArray();
        }
        elseif ($where !== null) {
            throw new AOOSException($this->core(), $this->tr("wrong_type"));
        }

        foreach ($this->_flags as $column => $flag) {
            if ($flag & AOOSMODEL_FLAG_FROM_DATABASE) {
                $fields[] = $column;
            }
        }

        try {
            $rows = $this->_storage->select($fields, $where, $order, $limit);
        } catch (AOOSException $e) {
            throw $e;
        }
        foreach ($rows as $row) {
            $r = new AOOSModelRow($this);
            $r->fromArray($row);
            $this->_data[] = $r;
            $this->_rows++;
        }
        return true;
    }

    public function create() {
        $flags = array();
        foreach ($this->_flags as $column => $flag) {
            if ($flag & AOOSMODEL_FLAG_FROM_DATABASE) {
                $flags[$column] = $flag;
            }
        }
        $types = array_intersect_key($this->_types, $flags);
        return $this->_storage->create($types, $flags);
    }

    public function drop() {
        return $this->_storage->drop();
    }

    public function save() {
        foreach ($this->data() as $row) {
            switch ($row->flag()) {
            case(AOOSMODELROW_PRIVATE_INSERT):
                $this->_storage->insert($row->toDatabaseArray());
                break;
            case(AOOSMODELROW_PRIVATE_UPDATE):
                $this->_storage->update($row->toDatabaseArray(), $row->old(), array(0,1));
                break;
            case(AOOSMODELROW_PRIVATE_REMOVE):
                $this->_storage->remove($row->toDatabaseArray(), array(0,1));
                break;
            }
            $row->setFlag(0);
        }
        return true;
    }
}

class AOOSModelRow extends AOOSModule implements ArrayAccess{
    private $_data          = array();
    private $_flag          = 0;
    private $_old           = null;

    public function __construct($parent, $value = null, $flag = 0) {
        parent::__construct($parent);
        $this->setFlag($flag);
        $this->_data = $this->parent()->defaultValues();
        if (is_array($value)) {
            $values = array_intersect_key($value, $this->_data);
            foreach ($values as $index => $val) {
                $this->_setData($index, $value[$index]);
            }
        }
    }

    public function __set($name, $value) {
        try {
            $ret = $this->_setData($name, $value);
        } catch (AOOSException $e) {
            throw $e;
        }
        return $ret;
    }

    public function __get($name) {
        if (!$this->parent()->inColumnIndex($name)) {
            throw new AOOSException($this->core(), $this->tr("index_error"));
        }
        return $this->_data[$name];
    }

    public function __toString() {
        return print_r($this->_data, true);
    }

    public function offsetSet($name, $value) {
        return $this->$name = $value;
    }

    public function offsetGet($name) {
        return $this->$name;
    }

    public function offsetExists($name) {
        return $this->parent()->inColumnIndex($name);
    }

    public function offsetUnset($name) {
        if (!$this->parent()->inColumnIndex($name)) {
            throw new AOOSException($this->core(), $this->tr("index_error"));
        }
        $this->$name = null;
        return true;
    }

    public function match($match) {
        $matches = array_intersect_key($match, $this->_data);
        foreach ($matches as $key => $val) {
            if (!preg_match("/".$val."/", $this->$key)) {
                return false;
            }
        }
        return true;
    }

    public function toArray() {
        $data = $this->_data;
        return $data;
    }

    public function toDatabaseArray($data = null) {
        if ($data == null) {
            $data = $this->_data;
        }
        foreach ($data as $key => $value) {
            $flags =$this->parent()->getFlags($key);
            if (!($flags & AOOSMODEL_FLAG_FROM_DATABASE) || ($flags & AOOSMODEL_FLAG_PRIMARY_KEY) || $this->_is_empty($key, $value)) {
                unset($data[$key]);
                continue;
            }
            $type = $this->parent()->getType($key);
            if ($type == AOOSMODEL_TYPE_STRING || $type == AOOSMODEL_TYPE_TEXT) {
                $data[$key] = "'".$value."'";
            } elseif ($type == AOOSMODEL_TYPE_BOOLEAN) {
                $data[$key] = $value ? 1 : 0;
            }
        }
        return $data;
    }


    public function fromArray($value) {
        $this->_data = $this->parent()->defaultValues();
        $values = array_intersect_key($value, $this->_data);
        foreach ($values as $key => $value) {
            $this->_data[$key] = $value;
        }
        return true;
    }

    public function old($db = true) {
        return $db ? $this->toDatabaseArray($this->_old) : $this->_old;
    }

    public function save() {
        return $this->parent()->save();
    }

    public function setFlag($flag) {
        if (!is_integer($flag)) {
            throw new AOOSException($this->core(), $this->tr("not_integer"));
        }
        $this->_flag = $flag;
        return true;
    }

    public function flag() {
        return $this->_flag;
    }

    /* ------ Private functions ----- */
    private function _checkType($name, $value) {
        $type = $this->parent()->getType($name);
        switch($type) {
        case(AOOSMODEL_TYPE_STRING):
        case(AOOSMODEL_TYPE_TEXT):
            return is_string($value);
            break;
        case(AOOSMODEL_TYPE_INTEGER):
            $value = (int) $value;
            return is_integer($value);
            break;
        case(AOOSMODEL_TYPE_BOOLEAN):
            return is_bool($value);
            break;
        case(AOOSMODEL_TYPE_UNKNOWN):
            return true;
            break;
        default:
            return false;
            break;
        }
    }

    private function _checkFlags($name, $value) {
        $flags = $this->parent()->getFlags($name);
        if ($flags === false) {
            return true;
        }
        if ($flags & AOOSMODEL_FLAG_UNIQUE) {
            $column = $this->parent()->getColumn($name);
            if (in_array($value, $column)) {
                return false;
            }
        }
        return true;
    }

    private function _doProperties($name, $value) {
        $properties = $this->parent()->getProperties($name);
        if ($properties === false) {
            return $value;
        }
        if ($properties & AOOSMODEL_PROP_TIME) {
            $value = time();
            return $value;
        }
        if ($properties & AOOSMODEL_PROP_HASH) {
            $value = hash("sha256", $value);
        }
        if ($properties & AOOSMODEL_PROP_NOHTML) {
            $value = htmlspecialchars($value);
        }
        if ($properties & AOOSMODEL_PROP_ESCAPE) {
            $value = mysql_real_escape_string($value);
        }
        if ($properties & AOOSMODEL_PROP_STRIP) {
            $value = rtrim(trim($value));
        }
        return $value;
    }

    private function _doConstraints($name, $value) {
        $constraints = $this->parent()->getConstraints($name);
        if ($constraints === false) {
            return $value;
        }
        foreach ($constraints as $constraint) {
            if (!call_user_func(array($constraint[0], "check"), $constraint[1], $value)) {
                continue;
            }
            $value = call_user_func(array($constraint[0], "execute"), $constraint[1], $value);
            if (false === $value) {
                throw new AOOSException($this->core(), $this->tr("constraint_fail"), $name);
                return false;
            }
        }
        return $value;
    }

    private function _setData($name, $value) {
        if ($this->flag() == AOOSMODELROW_PRIVATE_REMOVE) {
            return false;
        }
        if (!$this->parent()->inColumnIndex($name)) {
            throw new AOOSException($this->core(), $this->tr("index_error"));
        }
        if (!$this->_checkType($name, $value)) {
            throw new AOOSException($this->core(), $this->tr("wrong_type"), $name);
        }
        try {
            $value = $this->_doProperties($name, $value);
            $value = $this->_doConstraints($name, $value);
        } catch (AOOSException $e) {
            throw $e;
        }
        if (!$this->_checkFlags($name, $value)) {
            throw new AOOSException($this->core(), $this->tr("flags_failed"), $name);
        }
        if ($this->flag() == 0) {
            $this->setFlag(AOOSMODELROW_PRIVATE_UPDATE);
            $this->_old = $this->_data;
        }
        $this->_data[$name] = $value;
        return true;
    }

    private function _is_empty($key, $value) {
        $t = $this->parent()->getType($key);
        switch($t) {
        case(AOOSMODEL_TYPE_STRING):
        case(AOOSMODEL_TYPE_TEXT):
            return $value == "";
            break;
        case(AOOSMODEL_TYPE_INTEGER):
            return !is_integer($value);
            break;
        default:
            return false;
            break;
        }
    }
}
