<?php
$lang["en"]["unknown_module"] = "Unknown module";
$lang["en"]["not_string"] = "Not string";
$lang["en"]["not_array"] = "Not array";
$lang["en"]["value_not_in_row"] = "The given value is not in the given row";

$lang["en"]["row_out_of_bounds"] = "Selected row is out of bounds";
$lang["en"]["given_value"] = "Given value";
$lang["en"]["login_error"] = "Error occured during login";
$lang["en"]["error"] = "error";

$lang["en"]["mysql_query_fail"] = "Mysql query failed!";
$lang["en"]["mysql_close_fail"] = "Closing mysql resource failed!";

// AOOSModel
$lang["en"]["no_rows_found"] = "No rows were found";
$lang["en"]["couldnt_select_values"] = "The model could not be populated due to errors";
?>
