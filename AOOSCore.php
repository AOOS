<?php

/**
 * AOOSCore
 *
 * The core class of AOOS
 *
 * @author Sebastian Skejø
 */

class AOOSCore
{
    private $_exceptions    = array();
    private $_modules       = null;
    private $_settings      = array();
    private $_transStrings  = array();
    private $_log           = array();

    /**
     * Default constructor. An AOOSException is thrown if the language defined in $settings["lang"] isn't found the the 
     * array keys of the translated strings defined in lang.php.
     */
    public function __construct() {
        // Settings
        require("settings.php");
        $this->_settings = $settings; /* Set in settings.php */

        // Translated strings
        require("lang.php");
        $l = $this->getSetting("lang");
        if (in_array($l, array_keys($lang))) /* $lang set in lang.php */
            $this->_transStrings = $lang[$l];
        

        $m = new AOOSModel($this);
        $m->setColumnIndex(array(
            "NAME",
            "INSTANCE"
        ));
        $m->setProperties("NAME", AOOSMODEL_TYPE_STRING);
        $m->setProperties("INSTANCE", AOOSMODEL_TYPE_UNKNOWN);
        $this->_modules = $m;

        $this->log("Created AOOSCore");
    }

   /**
    * Logs the string with a timestamp
    * @param $str The string to log
    */
    public function log($str)
    {
        $this->_log[] = array(
            "STRING" => $str,
            "TIMESTAMP" => microtime()
        );
    }

    /**
     * Returns a string containing the log
     */
    public function log2str() {
        $str = "";
        foreach ($this->_log as $num => $entry) {
            $str .= "<strong>".$num.":</strong> ".$entry["STRING"]." at ".$entry["TIMESTAMP"]."<br />";
        }
        return $str;
    }

    /**
     * Sets an exception
     * @param $e The exception
     */
    public function setException($e)
    {
        $this->_exceptions[$e->getName()] = $e;
        if ($e->getEcode() == 0)
        {
            exit($e);
        }

        return true;
    }

    /**
     * Prints all exceptions
     */
    public function printExceptions()
    {
        $str = "<div class=\"Exceptions\">";
        foreach ($this->_exceptions as $e)
        {
            $str .= $e;
        }
        $str .= "</div>";
        print $str;
    }

    /**
     * Returns the value of the the given setting. Throws an AOOSException if the setting key isn't found.
     * @param $name Name of the setting
     * @param $module This is used to fetch per-module settings. If null is given a global setting is fetched.
     * @return string
     */
    public function getSetting($name, $module = null)
    {
        $a = $this->_settings;
        if ($module != null) {
            $a = $a[$module];
        }
        if (!in_array($name, array_keys($a))) {
            throw new AOOSException($this, "Settingkey not found", "Key: ".$name, true, 2);
        }
        return $a[$name];
    }

    /**
     * Used by AOOSModule to get translated string. SHOULD NOT BE ACCESSED MANUALLY! In order to translate a string use 
     * the AOOSModule::tr() function. Throws an AOOSException if the string isn't found.
     * @param $string The string to translate
     * @param $module This is used to fetch translated strings per-module. If a module name is given the translated 
     * string will be fetched from this module. If null is given, the string will be fetched from /lang.php
     * @return string
     */
    public function getTranslatedString($string, $module = null)
    {
        $a = $this->_transStrings;
        if ($module !== null) {
            $a = $a[$module];
        }
        if (!in_array($string, array_keys($a))) {
            return $string;
        }
        return $a[$string];
    }

    public function tr($string) {
        return $this->getTranslatedString($string);
    }

    /**
     * Loads $module. If the module couldn't be loaded an AOOSException is thrown. If the module is already loaded it is 
     * reloaded. If the module is successfully loaded the newly created instance is returned.
     * @param $module Name of the module
     * @return AOOSModule
     * @sa getModule
     */
    public function loadModule($module) {
        // Is module dir existing?
        $dir = $this->getSetting("module_dir").$module."/";
        if (!is_dir($dir)) {
            throw new AOOSException($this, $this->getTranslatedString("unknown_module"));
        }
        // Is module file existing?
        $path = $dir.$module.$this->getSetting("extension");
        if (!file_exists($path)) {
            throw new AOOSException($this, $this->getTranslatedString("module_unloadable"), "File error");
        }
        require_once($path);

        // Check dependencies
        $deps = call_user_func(array($module, "dependencies"));
        foreach ($deps as $dep) {
            if (!$this->getModule($dep)) {
                try {
                    $this->loadModule($dep);
                } catch (AOOSException $e) {
                    throw new AOOSException($this, $this->getTranslatedString("module_unloadable"));
                    return false;
                }
            }
        }

        // Load module settings
        $sf = $dir."settings".$this->getSetting("extension");
        if (file_exists($sf)) {
            require($sf);
            $this->_settings[$module] = $settings;
        }

        // Load module strings
        $lf = $dir."lang".$this->getSetting("extension");
        if (file_exists($lf)) {
            require($lf);
            $this->_transStrings[$module] = $lang;
        }

        // Create module instance
        $instance = new $module($this);
        if (($dm = $instance->dataModelDefinition()) !== 0) {
            $instance->setDataModel($dm);
        }
        $this->modules()->appendRow(array("NAME" => $module, "INSTANCE" => $instance));
        $instance->setName($module);

        // Classes and files used in module
        self::register($module, $path);
        foreach ($instance->files() as $class => $path) {
            self::register($class, $dir.$path);
        }

        return $instance;
    }

    /**
     * Returns the instance of the given $module. If this isn't instantiated yet, getModule will try to load it.
     * an AOOSException if loadModule fails.
     * @param $module Name of the module
     * @return AOOSModule|false
     * @sa loadModule
     */
    public function getModule($module) {
        $row = $this->modules()->find(array("NAME" => $module));
        return $row && $row->INSTANCE ? $row->INSTANCE : $this->loadModule($module);
    }

    /**
     * Registers a path to a certain class
     * @param $class Class name
     * @param $path The path relative to the root directory
     */
    static public function register($class, $path) {
        $_SESSION["files"][$path] = $class; // Use paths as keys so that we can handle similarly named classes from different modules
        return true;
    }

    /**
     * Returns the AOOSModel containing all module names, dependencies and instances.
     * @return AOOSModel
     */
    public function modules()
    {
        return $this->_modules;
    }

    /**
     * Simply returs a string containing "AOOSCore".
     * \internal This could be better
     */
    public function __toString() {
        return "AOOSCore";
    }
}
// vim: number
?>
