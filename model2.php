<?php
session_start();
require_once("AOOSCore.php");
require_once("AOOSModel2.php");
require_once("AOOSModule.php");
require_once("AOOSException.php");

function __autoload($class) {
    $rootfiles = array(
        "AOOSModule"        => "AOOSModule", 
        "AOOSModel"         => "AOOSModel2",
        "AOOSCore"          => "AOOSCore",
        "AOOSException"     => "AOOSException",
        "AOOSTypeException" => "AOOSException",
        "AOOSLangException" => "AOOSException",
        "MatchConstraint"   => "lib/AOOSModelConstraints"
    );
    require("settings.php");

    if (in_array($class, array_keys($rootfiles))) {
        require_once($rootfiles[$class].$settings["extension"]);
    }
    elseif (substr($class, 0, 4) == "AOOS") {
        require_once("lib/".$class.$settings["extension"]);
    }
    else
        require_once($settings["module_dir"].$class."/".$class.$settings["extension"]);
}

try {
    $c = new AOOSCore();
    $m = new AOOSModel($c);
    $m->setColumnIndex(array("foo", "bar", "baz"));
    $m->setSource("mysql");
    $m->setTable("Test");

/*    $m->setProperties("foo", AOOSMODEL_TYPE_INTEGER, 0, AOOSMODEL_FLAG_FROM_DATABASE);
    $m->setProperties("bar", AOOSMODEL_TYPE_STRING, 0, AOOSMODEL_FLAG_FROM_DATABASE);
    for ($i= 0; $i < 20; $i++) {
        $m->appendRow(array("bar" => $i, "foo" => $i*2));
    }*/

/*    $m->populate(array("foo" => 0));
    print $m->rows();*/

/*    $m->addConstraint("bar", "Match", array("/^[a-z0-9]+$/"));
    $m->addConstraint("foo", "Range", array(5,10));
    $m->populate();
    $r = $m->getRow(0);
    $r->foo = 13;
    $m->save();
    print "<hr />";
    print $m->getRow(0);*/

    $m->setProperties("foo", AOOSMODEL_TYPE_STRING, AOOSMODEL_PROP_NOHTML, AOOSMODEL_FLAG_UNIQUE);
    $m->setProperties("bar", AOOSMODEL_TYPE_BOOLEAN);
    $m->setProperties("baz", AOOSMODEL_TYPE_TEXT, AOOSMODEL_PROP_ESCAPE|AOOSMODEL_PROP_STRIP);

    $m->appendRow(array(
        "foo" => "<strong>Her skulle der gerne ikke være html</strong>",
        "bar" => false,
        "baz" => " Han sagde 'Denne streng er \"escaped\"!'\t\n   "));
    $m->appendRow(array(
        "foo" => "<strong>Her skulle der gerne ike være html</strong>",
        "bar" => false,
        "baz" => " Han sagde 'Denne streng er \"escaped\"!'\t\n   "));
    print "'".$m->getRow(0)->baz."'";
} catch (AOOSException $e) {
    print $e;
}
?>
